﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using TMPro;

public class TextFromJson : MonoBehaviour
{
    public string linkJSON;
    public int index;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI emailText;

    private void Awake()
    {
        WWW req = new WWW(linkJSON);
        StartCoroutine(ProcessRequestJSON(req));
    }

    IEnumerator ProcessRequestJSON(WWW jsonReq)
    {
        yield return jsonReq;

        emailText.text = GetPersonEmail(jsonReq.text);
        nameText.text = GetPersonName(jsonReq.text);
    }

    private string GetPersonName(string json)
    {
        List<Person> PersonInfo = JsonConvert.DeserializeObject<List<Person>>(json);
        return PersonInfo[index].name.ToString();
    }

    private string GetPersonEmail(string json)
    {
        List<Person> PersonInfo = JsonConvert.DeserializeObject<List<Person>>(json);
        return PersonInfo[index].email.ToString();
    }
}



public class Person
{
    public string name;
    public string email;
    public Person (string name, string email)
    {
        this.name = name;
        this.email = email;
    }
}
